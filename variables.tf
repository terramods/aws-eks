variable "aws_region" {
  type = string
  default = "sa-east-1"
}

variable "aws_profile" {
  type = string
}

# 

variable "owner" {
  type = string
  default = ""
}

variable "project" {
  type = string
  default = ""
}

variable "product" {
  type = string
  default = ""
}

variable "environment" {
  type = string
  default = ""
}

#

variable "endpoint_private_access" {
  type = bool
  default = false
}

variable "endpoint_public_access" {
  type = bool
  default = true
}

variable "eks_cluster_role_arn" {
  type = string
}

variable "eks_nodegroup_role_arn" {
  type = string
}

variable "public_subnet_ids" {
  type = list
  default = []
}

variable "private_subnet_ids" {
  type = list
  default = []
}

variable "disk_size" {
  type = number
  default = 20
}

variable "instance_type" {
  type = string
  default = "t3.medium"
}

variable "nodegroup_desired_size" {
  type = number
  default = 3
}

variable "nodegroup_min_size" {
  type = number
  default = 3
}

variable "nodegroup_max_size" {
  type = number
  default = 3
}