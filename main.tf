provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
  version = "~> 2.35"
}

terraform {
  required_version = ">= 0.12.21"
  backend "s3" {}
}

locals {
  base_name = "${lower(var.owner)}-${lower(var.project)}${var.product != "" ? format("-%s", lower(var.product)) : ""}-${lower(var.environment)}"
  base_tags = {
    Deployer = "Terraform"
    Owner = title(var.owner)
    Project = title(var.project)
  }
}

resource "aws_eks_cluster" "tm_eks_cluster" {
  name     = "${local.base_name}-eks-cluster"
  role_arn = var.eks_cluster_role_arn
  vpc_config {
    subnet_ids              = concat(var.public_subnet_ids, var.private_subnet_ids)
    endpoint_private_access = var.endpoint_private_access
    endpoint_public_access  = var.endpoint_public_access
  }

  tags = merge({
    "Name" = "${local.base_name}-eks-cluster"
  },
  local.base_tags)
}

resource "aws_cloudwatch_log_group" "tm_cloudwatch_log_group" {
  name              = "/aws/eks/${local.base_name}-eks-cluster/cluster"
  retention_in_days = 7
}



resource "aws_eks_node_group" "tm_eks_node_group" {
  cluster_name    = aws_eks_cluster.tm_eks_cluster.name
  node_group_name = "${local.base_name}-eks-nodegroup"
  node_role_arn   = var.eks_nodegroup_role_arn
  subnet_ids      = var.private_subnet_ids
  disk_size       = var.disk_size
  instance_types   = [var.instance_type] 
  scaling_config {
    desired_size  = var.nodegroup_desired_size
    min_size      = var.nodegroup_min_size
    max_size      = var.nodegroup_max_size
  }

  depends_on = [aws_eks_cluster.tm_eks_cluster]

  tags = merge({
    "Name" = "${local.base_name}-eks-nodegroup"
  },
  local.base_tags)

  lifecycle {
    ignore_changes = [
      tags
    ]
  }
}